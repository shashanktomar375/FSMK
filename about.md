---
layout: page
title: About
permalink: /about/
---



Free Software Movement- Karnataka is a registered not-for-profit organisation. ‘Free Software Movement- Karnataka’ name symbolises the inclusiveness the organisation pushes in the society. Our primary objective is to create and spread awareness of Free Software technologies in different strata of society. 

A few activists from Bangalore decided to build the free software movement in Karnataka on the lines of a mass movement rather then the anarchist approach that existed in the global free software movement. These activists started working with pre-existing groups such as FSUG-Bangalore and FSF-India since the end of 2005 and were part of organising several activities and programs such as the GPLv3 conference in 2006, protests and programs related to patents in software, forming GLUGs in local engineering colleges, community computing centres, national conventions among others.

We, the Free Software Movement Team having been consistently working to build the Free Software Movement in Karnataka. Our work has been built on the work of the pioneers of Free Software in the country. We work closely with college students and encourage them to use, and help develop, Free Software. To make this student interaction, we set up college-level units called GNU/Linux User Groups (GLUGs). These GLUGs organise various technical sessions and events. One of our initiatives, a one of a kind for a Free Software organisation, is community center’s which aims for having inclusiveness. A community center is an initiative in a local community, owned by the community itself – it’s a self sustaining unit. Like all of our initiatives, these centers are volunteer driven. The objective of a community center is to help educate the local community – usually children – in the use of computers and the internet. Centers often also provide the members with assistance with academic subjects. There are multiple community centers in Bangalore. We hope to open more in different parts of Karnataka as well.

We are driven by volunteers, who by day are software engineers, students, academicians, or government officials, and by night are Free Software evangelists. We push organisations and individuals to use Free Software in their domain, and are pushing for localisation in the state to eliminate English language as a dependency to access the digital world. 





[centrarium]: https://github.com/bencentra/centrarium
[bencentra]: http://bencentra.com
[jekyll]: https://github.com/jekyll/jekyll
