---
layout: post
title: "Weekly Report"
date: 2014-09-01 17:37:38
category: updates
---

News

Balaji, Jeeva and Shijil  from FSMK Visited AC3 community centre Sunday(31st Aug) , initiated discussion on restarting the community centre. AC3 was the first community centre FSMK started , later due to many reason especially space issues centre was closed. This meeting has generated more excitement among all of us , students from AC3 are more excited and they have taken it as a challange re starting the center. Mani , Sarasu , Veeru , Sungeetha , Renuka , Vishal and Radhika attended meeting from AC3 side.

Read more about AC3 community centre.  
<https://www.gnu.org/education/edu-cases-india-ambedkar.html>  
<http://slumdweller.wikispaces.com/>

Upcoming activities Reported So far

GLUGDSCE - On 6th and and 7th September Hands on session on Python Basics

TOGGLE- On 4th September Hands on Session on Python Basics

What Happened..

#DSCEGLUG  
Conducted session on DBMS using MySQL on 30th Aug - Handled by Rakesh P Gouda

#TFSCBMSIT  
Conducted Theory + Hands on session on python Basics  on 30th Aug  
[http://sosaysharis.wordpress.com/2014/08/31/weekend-pythonexpress-worksh...][1]

 [1]: http://sosaysharis.wordpress.com/2014/08/31/weekend-pythonexpress-workshop-at-bmsit-bangalore/

conducted Session on Foss and why it should matter to students (by Nitesh) and GSoC by (Bharath) on 28th Aug

#MSRITGLUG  
Shashank gave a talk at MSRIT for 3rd and 5th Sem students from EEE department. Overall there were around 80 of them. Ahalya from MSRIT who also had attended the camp had coordinated the talk

#BNMITGLUG  
Free Software introduction session at BNMIT -  It was coordinated by Arpitha from 3rd year who had participated in Summer Camp-14. 15 students attended the session.

#SUNDAYSCHOOL

Conducted session on Debian packaging for diaspora Gems- Nitesh handled the session
