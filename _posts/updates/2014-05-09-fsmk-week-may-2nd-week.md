---
layout: post
title: "FSMK This Week  May 2nd Week"
date: 2014-05-09 15:41:37
category: updates
---

*Introduction to Unix at SVCE College on 10/05/2014. Vijay Kulkarnny will handle the session.

*FMSK Camp content is finalized

>Introduction to GNU/Linux commands and environment. Including  
Shell scripts

>Linux networking. Understanding various networking concepts and  
network protocols, SSH, DNS, Web Server, etc.

>Introduction to HTML and CSS,

>(One and Half day): Using HTML5 and javascript to develop  
mobile application

>(One and Half day): Developing Web Application using node.js  
Day 7: Introduction to cryptography and protecting your online  
identity tools.

>Project work

*FSMK associated Summer Camp for school kids is going on . Other co organizers are  DYFI,BGVS and Karnataka Yuvajana Sangha  
mainly amigo center students volunteering the camp.  
Monday is last day for camp. Detail report will be sent next week with colorful photos :)  
38 students are attending the camp
