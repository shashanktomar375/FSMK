---
layout: post
title: "FSMK offers scholarships to subsidize the camp fee for deserving students"
date: 2014-07-17 07:06:08
category: updates
---

As FSMK has been organizing free software technology camps for the last 2 years, this time FSMK team have offered to provide scholarships which would help deserving students to get subsidized in the camp fee. As a not-for-profit organization, FSMK collects fee from participant towards the actual expenses food, registration kit, etc. However, there are students who are meritorious but can not afford to pay the fee due to their poor economic and social background.

In this background, affordability of technology by the marginalized sections should also be given equal emphasis in our endeavor to build free software movement, this scholarship initiative is being experimented.This thinking is nothing new to those who have already been familiar with the FSMK's initiative of community computing centres presently run at 3 places in Bangalore to teach free software and science based learning to students of marginalized communities.

Criteria followed for offering such scholarship is : the students staying in government hostels are recommended for the fee waiver in the first place because the govt dept has already verified their merit along with their economic and social background while giving admission.In the second place, Engg students have been included from farming community and low income groups whose economic conditions are not good.
