---
layout: post
title: "FSMK Newsletter Volume 08"
date: 2013-04-08 18:31:05
category: updates
---

Hurrraaaaaayyyyyyyyyyyyyyyyyyyyyyyy!!!!!!

Here comes the revamped FSMK Newsletter, after quite a delay.  
With apologies, we assure to be more consistent this time around :)

Working during this period of time, FSMK has identified numerous tasks to be completed in the Free Software domain in Karnataka.

FSMK's current General Body has formed 7 different committees, each to take up prominent tasks; rejuvenating the Newsletter has been one of the primary tasks.

We have clear understanding of the important role, this Newsletter would be playing to enhance our reach to a larger audience. The Newsletter, more importantly should reach people who aren't aware of FSMK.

While we see the Newsletter as an interface to the students in Engineering colleges, Arts and Science Colleges, Journalism Colleges, Management Institutes, etc, we are not limiting ourselves to any section of people. FSMK wants to work with everyone who is a potential user of Computers and Software.

Please share and spread this Newsletter to as many people as possible.  
For, Sharing is Growing :)

Help many more people to join the Free Software Movement Karnataka.

We are awaiting your feedback of this reinvigorated attempt from us.  
Not just feedback, but more ideas and many more thoughts, in the form of articles.

Free Software is the future..  
Future is Ours..

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue8_July_2011.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 08</a>

 

Regards,  
FSMK Newsletter Team

 
