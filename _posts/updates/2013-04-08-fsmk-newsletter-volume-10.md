---
layout: post
title: "FSMK Newsletter Volume 10"
date: 2013-04-08 18:35:41
category: updates
---

The start of the year 2012 brings in a lot of new tasks and activities for FSMK, and we want to keep ourselves busy for the rest of the year with more enthusiasm and more encouragement.

We present to you FSMK Newsletter - January 2012.

In this edition, we look back into some of the challenges FSMK faced during the course of our journey and would like to hear suggestions to overcome them. Other articles written focus on the impending threats to internet freedom. In the midst of disasters like <a href="http://sopastrike.com/">SOPA and PIPA</a>, we as Free Software enthusiasts, or even as Internet users, or as plain human beings who value our freedom, should learn and understand the traps these bills create and fight our way out of them.  As January 18th saw the <strong>Largest Online protest in History</strong>, the articles will give an insight the legislations and the impact they have on everyone.

It is also highly encouraging to have contributions to the newsletter from students who are part of prestigious institutions in India and abroad. FSMK takes pride in providing a platform for students to share their time and knowledge with fellow students and enthusiasts.

Last but not the least, Jeeva details the progress of the community computing center and how it has changed the lives of people benefiting from the initiative.

As always, give it a read, and spread it further. Looking forward to your feedback.  
Not just feedback, but more ideas and many more thoughts, in the form of articles.

Free Software is the future..  
Future is Ours..

 

<a href="http://fsmk.org/sites/default/files/NewsLetter/FSMK%20Newsletter%20Volume%2001.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 10</a>

 

Regards,  
FSMK Newsletter Team  
editor@fsmk.org
