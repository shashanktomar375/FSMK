---
layout: post
title: "Sunday School Gaming and Game Development on Gnu/Linux"
date: 2015-03-13 20:23:33
category: events
---

*Introduction to gaming platforms available on GNU/Linux   
*Game development concepts  
*Development platforms available  
*LAN party over WiFi  

by [Ajith Demon][1]

 [1]: https://www.facebook.com/ajith.demon

Sunday, February 23, 2014at 2:00pm - 4:00pm  
@FSMK Office
