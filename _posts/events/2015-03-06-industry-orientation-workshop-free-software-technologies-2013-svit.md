---
layout: post
title: "Industry Orientation Workshop on Free Software Technologies 2013 @SVIT"
date: 2015-03-06 18:06:44
category: events
---

<table>
  <tbody>
    <tr>
      <td colspan="2">
        Free Software movement Karnataka (FSMK) is organizing 5 days Industry Orientation workshop on preparing participants on Free Software technologies for the software industry. Currently software industry is actively adopting free software to reduce the cost and have much more freedom to modify the software to suit their needs. This has resulted in industry's growing need of software professionals with expertise in free software technologies. This workshop is to ensure that participants are trained on various free software technologies so that they are better prepared for the industry.<br /> The workshop will be a hands on workshop where students will gain knowledge on various technologies like Java, Python, Networking in Linux, Web Programming. The sessions will be conducted by industry professionals with vast amount of work experience. At the end of the workshop, participants will be equipped with knowledge of free software in various domains so that they can choose whichever domain they are interested in and start working on it right during their college days.
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <strong><u>Technical Syllabus</u></strong> The Technical part of the workshop will have three main parts.
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <strong><u>Free Software Technologies (6 hr/day):</u></strong> This part will deal with sessions on various free software technologies. The idea of this part is to give enough information to students about various technologies so that they can then choose their domain of interest and continue learning it.<br />   <ol>
          <li>
            <strong>Friendship with GNU/Linux system</strong> <ol>
              <li>
                Introduction to GNU/Linux shell commands
              </li>
              <li>
                Understanding GNU/Linux file systems
              </li>
              <li>
                Understanding GNU/Linux processes
              </li>
              <li>
                Becoming familiar with vi/vim editor
              </li>
            </ol>
          </li>

          <li>
            <strong>Networking in UNIX</strong> <ol>
              <li>
                Introduction to basic IP Address/gateways/routing
              </li>
              <li>
                Configuring services like Apache2/Dynamic-DNS/ssh
              </li>
              <li>
                Understanding networking commands like telnet, ssh, ping, route,ifconfig
              </li>
            </ol>
          </li>

          <li>
            <strong>Java</strong> <ol>
              <li>
                Introduction to OOPS concept using Java
              </li>
              <li>
                Brief introduction to developing android apps using Java
              </li>
            </ol>
          </li>

          <li>
            <strong>Python</strong> <ol>
              <li>
                Introduction to python
              </li>
              <li>
                Developing desktop applications using Py/GTK
              </li>
            </ol>
          </li>

          <li>
            <strong>Web Programming in LAMP</strong> <ol>
              <li>
                Introduction to LAMP stack
              </li>
              <li>
                Developing web applications using PHP
              </li>
            </ol>
          </li>
        </ol>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <strong><u>Free Software Contributions(1 hr/day):</u></strong> This part will deal with sessions on how to contribute to free software. It will be a one hour per day session touching on various aspects of free software contribution. <ol>
          <li>
            <strong>Free Software Introduction</strong> <ol>
              <li>
                History, significance
              </li>
              <li>
                Licenses
              </li>
              <li>
                Migration
              </li>
            </ol>
          </li>

          <li>
            <strong>Interaction with free software community</strong> <ol>
              <li>
                IRC
              </li>
              <li>
                Mailing list
              </li>
              <li>
                Etiquette
              </li>
            </ol>
          </li>

          <li>
            <strong>Bug triaging</strong> <ol>
              <li>
                Reporting bugs
              </li>
              <li>
                Bug triaging
              </li>
            </ol>
          </li>

          <li>
            <strong>Where is the source code</strong> <ol>
              <li>
                Concept of Code revisions
              </li>
              <li>
                Tools like git
              </li>
              <li>
                Free online project hosting like github
              </li>
            </ol>
          </li>

          <li>
            <strong>Localization</strong> <ol>
              <li>
                Need for localization
              </li>
              <li>
                .po files
              </li>
              <li>
                Tools for localization
              </li>
            </ol>
          </li>
        </ol>
      </td>
    </tr>

    <tr>
      <td colspan="2">
        <strong><u>Free Software in Industry(1 hr/day):</u></strong> This part will have sessions about the increasing trend of usage of free software in the industry. Various industry personnel with vast amount of experience will be joining us to discuss on how they have adopted free software in their industry. It will be a one hour session per day. Few listed topics are as below: <ol>
          <li>
            Entrepreneurship using Free Software
          </li>
          <li>
            Free Software in Quality Testing industry
          </li>
          <li>
            Free Software in System Administration
          </li>
          <li>
            Free Software for Small/Medium Enterprises
          </li>
          <li>
            Concept of freedom in Hardware
          </li>
          <li>
            Free Software in Enterprise Server Market
          </li>
          <li>
            Free Software in Cloud Computing
          </li>
        </ol>
      </td>
    </tr>
  </tbody>
</table>
