---
layout: post
title: "Understanding the Database concepts"
date: 2015-03-13 20:01:30
category: events
---

Understanding the Database concepts  
: "Views, Temporary tables, Reorg Utility and Overview of Stored Procedures and Triggers"  

Prerequisistes:  
Basic knowledge of Query writing.  

LAMP package installed, along with PHPmyadmin.  

LAMP:  
<a href="https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu" rel="nofollow nofollow" style="color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; font-size: 14px; line-height: 18.7600002288818px;" target="_blank">https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu</a>  

PHPmyadmin:  

sudo apt-get install phpmyadmin

Sunday, November 16, 2014at 11:00am  
@Free Software Movement Karnataka No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 56003
