---
layout: post
title: "Sunday School on GIMP"
date: 2015-03-13 20:05:19
category: events
---

GIMP is an acronym for GNU Image Manipulation Program. It is a freely distributed program for such tasks as photo retouching, image composition and image authoring.

Sunday, November 2, 2014at 11:00am  
@fsmk office
