---
layout: post
title: "FreeSoftware Talks from RMS and Eben Moglen"
date: 2013-02-10 10:30:59
category: events
---

Here are the links to two videos by Richard M Stallman and Eben Molgen. They talks in detail about the free software movment.

[Eben Moglen at Christ University][1]  
[RMS at Christ University ][2]

 [1]: http://blip.tv/file/1618770
 [2]: http://blip.tv/file/1618916

Whether you are a free software activist, sympathizer or opponent it is worth to refer.

 
