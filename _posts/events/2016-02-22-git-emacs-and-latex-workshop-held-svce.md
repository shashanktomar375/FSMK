---
layout: post
title: " Git, emacs and Latex workshop held at SVCE"
date: 2016-02-22 17:31:26
category: events
---

Git, emacs and Latex workshop held at SVCE 13th Feb 2016. Around 50 students attended the workshop and it was completely hands on. Vikram Vincent handled the workshop. The workshop was organized by FOCUS GLUG a unit of FSMK in SVCE.
