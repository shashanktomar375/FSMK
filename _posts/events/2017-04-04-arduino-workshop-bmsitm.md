---
title:  Arduino Workshop at BMSIT&M
category: events
layout: post
date: 2017-04-04 23:47:00 +0530
startdate: 2017-04-07 08:30:00 +0530
enddate: 2017-04-21 06:30:00 +0530
discourse_topic_id: 588
---

Greetings!

The Free Software Club (BMSIT&M) is having a hands-on session on Arduino conducted by Anup Pravin.

This is going to be a two day workshop.

    Day one, 7th April, Friday, will cover topics like Freedom Hardware, Adruino coding basics, understanding actuators and sensors using motors.

    Day two, 21st April, Friday, will cover introduction to IoT concepts, interfacing an Arduino with a web client, data logging and control through a web client.

**Time:** 08:30AM - 04:30PM on both days

**Venue:** Drawing Hall 1, Ground Floor, BSN Block, BMSIT&M

**Registration fee:** Rs. 300 per head

For registration and other details check out http://register.thefreesoftware.club3

For any queries, contact:
<br>
**Melrick** (7760038110)
<br>
**Jayasurya** (8197360630)
