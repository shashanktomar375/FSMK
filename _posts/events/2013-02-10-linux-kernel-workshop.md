---
layout: post
title: "Linux Kernel Workshop"
date: 2013-02-10 02:20:37
category: events
---

Write up by Raghavendra

The much awaited encore workshop on ***'GNU tools and Linux Kernel'*** organised by Free Software Movement-Karnataka(FSMK) and IEEE driven by IBM Linux Technology Centre conducted at BMS College of Engineering, Bangalore,  on 9th and 10th May 2009. Around 100 People consisting of Students and Working professionals particpated in the Event. Team from IBM took sessions on programming tools on GNU/Linux platform, the overview of the Linux kernel and Linux configuration.

After a very formal inauguration, the technical session started at 11 AM. The sessions spread over the entire day upto 6 PM covered wide range of topics related to the Kernel Technology and associated GNU tools like GCC,GDB, and others. The hands on session sticking to their basic attributes were chaotic and confusing initially but mellowed down subsequently. Two main hands on sessions of 45 minutes each duration were conducted;one on Linux kernel building and its programming,while the second one on Filesystems. The speakers from IBM were very knowledgeable in whichever topic they handled.

After a good start on Saturday,the second day of the session continued with the same momentum, started off with the Free Software Philosophy by an inspiring FSMK volunteer. Talking about the knowledge barrier,the monopoly of Information products on the market as well as our freedom. Many who were unaware were instigated for sure.

IBM-LTC took train of good talks and sensible exercises continued even for the second day. Kernel primitives,kernel debugging tools,device drivers and so on. The interaction amongst the participants and the IBM-LTC people were the most fruitful. Mementos from IEEE are given to IBM-LTC volunteers for the sessions

FSMK had its stall where our T shirts, posters were sold and sample packets of our philosophy were handed out as pamphlets. FSMK volunteers had a very sensible discussion, touching upon the growth path of each of its members in this journey of Free Software and each ones role in it. Also FSMK Volunteers took sessions on Free Software Philosophy. FSMK volunteers had a real nice, at the same time very useful time interacting with various people from industry and academics. The Overwhelming response from the participants are really encouraging to conduct more of such events.
