---
layout: post
title: "Debian (and Ubuntu) Release Party"
date: 2015-03-13 20:44:37
category: events
---

Let's celebrate the latest release of Debian, the Universal Operating System. In spite of there being a few hundred GNU/Linux Distributions, Debian holds a place of its own. Not for being the mother of Ubuntu, but for the commitment it has to the idea of Free Software. Oh yeah, let's celebrate Ubuntu's release too.  

Agenda:  
1. Cake cutting   
2. Talk on "Why is Debian Important to Free Software Community?"  
3. Install fest - Debian and Ubuntu
