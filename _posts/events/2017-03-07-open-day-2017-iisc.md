---
title: Open Day 2017 at IISc
layout: post
category: events
date: 2017-03-07 15:32:00 +0530
startdate: 2017-03-04 09:00:00 +0530
enddate: 2017-03-04 18:00:00 +0530
discourse_topic_id: 538
---

### What is Open Day?
Open Day is an annual event organized by the Indian Institute of Science, Bengaluru to commemorate the birthday of its founder — Jamshedji Tata. Every department opens their doors to the general public, and the huge campus is abuzz with demonstrations, stalls, competitions, open laboratories and, of course, yummy food.

### What did we do?
FSMK in collaboration with the Computer Science and Automation department put up a stall dedicated to evangelizing free software.

#### Desk 1: Introduction to Free Software
We talked to participants about what Free Software (FS) is, the need for FS in modern technology and the various initiatives available to help support the cause. We also set up two laptops where people could leave their contact details with us.

#### Desk 2: Freedom Hardware
As part of our FSHM revival movement, we set up a model where we coded a smartwatch display connected to an arduino to connect to the local wifi and pull notifications from our phone. The display transitioned various FS slogans every two seconds. We also had another board that would emit a beeping sound controllable by the audience through a web page. It was a lot of fun.

The aim of this section was to show that it is very easy to start playing around with hardware. Needless to say this section was the crowd-puller. A lot of engineering students were interested in discussing IOT possibilities, and some of the younger ones even shared their experiences from similar projects they completed themselves.
Read more [here](https://discuss.fsmk.org/t/freedom-hardware-open-day-2017-iisc-bangalore/532).

#### Desk 3: L10NSPRINT #1
To complement FSMK's efforts of localizing our website for Kannada readers, we set up a stall to crowd-source translations for English strings used on the websiite. Free (libre + gratis) Distro discs were given to people who contributed. The stall saw very active participation from the crowd. The gamification of the activity yielded positive results and we can't wait to do this again.
Read more [here](https://discuss.fsmk.org/t/l10n-sprint-1-at-iisc-open-day-2017/530).

### Response and results
FSMK volunteers interacted with people from various walks of life. A number of students showed interest in our GLUG initiative and were eager to start one in their own colleges. L10n, too, received a lot of interest, as people realized the need to transcend language barriers. Most people hadn't thought much about these things at all, and were very interested in what we had to say.
With the hardware desk the goal was to make people realize how easy it is to get started, and we believe that we have reached that goal. A large number of people said they were interested in attending any future workshops we will hold.
