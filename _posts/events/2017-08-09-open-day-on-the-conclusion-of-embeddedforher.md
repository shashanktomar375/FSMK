---
title: Open day on the conclusion of EmbeddedforHer
category: events
layout: post
pinned: true
date: 2017-08-09 00:00:00 +0000
startdate:  2017-08-13 16:00:00 +0000
enddate: 2017-08-13 18:00:00 +0000
discourse_topic_id: 974
---

The first iteration of the Embedded for Her program will be concluded this week. To mark this, an open day has been scheduled. Members from the embedded systems and open hardware communities have been invited to interact with the participants.


Date: Sunday, 13th August

Venue: <a href="https://ikpeden.com/contact-us/" target="_blank">IKP Eden</a>

Time: 4pm - 6pm


Agenda:

5:00 pm - General report on EfH and related activities

5:20 pm - Interactive demonstration of completed projects by participants

6:20 pm - Tea break and networking

6:40 pm - Discussions about future activities and the involvement of the industry

7:00 pm - Conclusion



We'll see you there!
